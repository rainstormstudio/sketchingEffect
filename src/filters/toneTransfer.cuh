#ifndef TONE_TRANSFER_CUH
#define TONE_TRANSFER_CUH

#include "../utilities/utilities.cuh"

__global__ void toneTransfer(Color* buffer, int width, int height) {
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if ((x >= width) || (y >= height)) return;

    int idx = Utility::getIdx(x, y, width);

    Color color{0.0f};

    color = Color(glm::clamp(static_cast<int>((buffer[idx].r * 16.0f) + 0.5f) / 16.0f, 0.0f, 1.0f));

    buffer[idx] = color;
}

#endif
