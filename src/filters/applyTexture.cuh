#ifndef APPLY_TEXTURE_CUH
#define APPLY_TEXTURE_CUH

#include "../utilities/utilities.cuh"

__global__ void applyTexture(Color* buffer, Color* texture, int width, int height) {
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if ((x >= width) || (y >= height)) return;

    int idx = Utility::getIdx(x, y, width);
    
    buffer[idx] = Color(powf(texture[idx].r, 1.0f / ((glm::clamp(buffer[idx].r, 0.0f, 1.0f)) * 2.0f)));
    // buffer[idx] = texture[idx] * buffer[idx];
}

#endif
