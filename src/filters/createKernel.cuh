#ifndef CREATE_KERNEL_CUH
#define CREATE_KERNEL_CUH

#include "../utilities/utilities.cuh"

__global__ void createKernel(Color* kernel, int kernelSize, int direction) {
    for (int y = 0; y < kernelSize * 2 + 1; y++) {
        for (int x = 0; x < kernelSize * 2 + 1; x++) {
            int idx = Utility::getIdx(x, y, kernelSize * 2 + 1);
            kernel[idx] = Color(0.0f);

            if (direction == 0) {
                if (x == kernelSize) {
                    kernel[idx] = Color(1.0f);
                }
            } else {
                float f = tanf((direction / 8.0f - 0.5f) * PI) * (x - kernelSize) + kernelSize;
                if (y == static_cast<int>(f + 0.5f)) {
                    kernel[idx] = Color(1.0f);
                }
            }
        }
    }
}

#endif
