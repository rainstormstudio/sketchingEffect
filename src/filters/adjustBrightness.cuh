#ifndef ADJUST_BRIGHTNESS_CUH
#define ADJUST_BRIGHTNESS_CUH

#include "../utilities/utilities.cuh"

__global__ void adjustBrightness(Color* buffer, int width, int height) {
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if ((x >= width) || (y >= height)) return;

    int idx = Utility::getIdx(x, y, width);
    
    buffer[idx] = buffer[idx] * 0.8f + 0.2f;
}

#endif
