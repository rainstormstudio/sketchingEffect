#ifndef MIXTURE_CUH
#define MIXTURE_CUH

#include "../utilities/utilities.cuh"

__global__ void mixture(Color* buffer, Color* other, int width, int height) {
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if ((x >= width) || (y >= height)) return;

    int idx = Utility::getIdx(x, y, width);

    buffer[idx] = buffer[idx] * other[idx];
}

#endif
