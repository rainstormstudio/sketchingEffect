#ifndef INVERT_CUH
#define INVERT_CUH

#include "../utilities/utilities.cuh"

__global__ void invert(Color* buffer, int width, int height) {
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if ((x >= width) || (y >= height)) return;

    int idx = Utility::getIdx(x, y, width);

    Color out = Color(1.0f) - glm::clamp(buffer[idx], 0.0f, 1.0f);

    buffer[idx] = out;
}

#endif
