#ifndef GRADIENT_CUH
#define GRADIENT_CUH

#include "../utilities/utilities.cuh"

__global__ void gradient(Color* buffer, Color* output, int width, int height) {
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if ((x >= width) || (y >= height)) return;

    int idx = Utility::getIdx(x, y, width);

    Color out{0.0f};
    float deltaX, deltaY;

    if (x == width - 1) { deltaX = 0.0f; } 
    else { deltaX = buffer[Utility::getIdx(x + 1, y, width)].r - buffer[idx].r; }

    if (y == height - 1) { deltaY = 0.0f; }
    else { deltaY = buffer[Utility::getIdx(x, y + 1, width)].r - buffer[idx].r; }

    // float G = powf(deltaX * deltaX + deltaY * deltaY, 0.5f);
    float G = sqrtf(deltaX * deltaX + deltaY * deltaY);

    output[idx] = Color(G);
}

#endif
