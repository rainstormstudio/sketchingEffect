#ifndef CONVOLUTION_CUH
#define CONVOLUTION_CUH

#include "../utilities/utilities.cuh"

__global__ void convolution(Color* buffer, Color* conv, int width, int height, Color* kernel, int kernelSize) {
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if ((x >= width) || (y >= height)) return;

    Color C = Color(0.0f);
    for (int j = 0; j < kernelSize * 2 + 1; j ++) {
        for (int i = 0; i < kernelSize * 2 + 1; i ++) {
            int dx = i - kernelSize;
            int dy = j - kernelSize;
            int xx = x - dx;
            int yy = y - dy;
            if (xx >= 0 && xx < width && yy >= 0 && yy < height) {
                C += buffer[Utility::getIdx(xx, yy, width)] * kernel[Utility::getIdx(i, j, kernelSize * 2 + 1)];
            }
        }
    }

    conv[Utility::getIdx(x, y, width)] = C;
}

#endif
