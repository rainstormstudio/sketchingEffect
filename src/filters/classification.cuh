#ifndef CLASSIFICATION_CUH
#define CLASSIFICATION_CUH

#include "../utilities/utilities.cuh"

__global__ void classification(Color* conv, Color* gradient, int width, int height) {
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if ((x >= width) || (y >= height)) return;

    int idx = Utility::getIdx(x, y, width);

    float C = 0.0f;
    int maxIdx = 0;
    
    for (int i = 0; i < 8; i ++) {
        if (C < conv[i * width * height + idx].r) {
            C = conv[i * width * height + idx].r;
            maxIdx = i;
        }
        conv[i * width * height + idx] = Color(0.0f);
    }
    conv[maxIdx * width * height + idx] = gradient[idx];
}

#endif
