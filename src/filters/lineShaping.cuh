#ifndef LINE_SHAPING_CUH
#define LINE_SHAPING_CUH

#include "../utilities/utilities.cuh"

__global__ void lineShaping(Color* conv, Color* output, int width, int height) {
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if ((x >= width) || (y >= height)) return;

    int idx = Utility::getIdx(x, y, width);

    Color color{0.0f};

    for (int i = 0; i < 8; i ++) {
        color += conv[i * width * height + idx];
    }

    output[idx] = Color(glm::clamp(color.r * 0.6f + 0.4f, 0.0f, 1.0f));
}

#endif
