#ifndef GRAY_SCALE_CUH
#define GRAY_SCALE_CUH

#include "../utilities/utilities.cuh"

__global__ void grayScale(Color* buffer, int width, int height) {
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if ((x >= width) || (y >= height)) return;

    int idx = Utility::getIdx(x, y, width);

    Color out = Color(0.2126f * buffer[idx].r + 0.7152f * buffer[idx].g + 0.0722f * buffer[idx].b);

    buffer[idx] = out;
}

#endif
