/**
 * @file debug.cuh
 * @author Hongyu Ding (rainstormstudio@yahoo.com)
 * @brief This file defines functions for debugging purposes
 * @version 0.1
 * @date 2021-06-08
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef DEBUG_CUH
#define DEBUG_CUH

#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <chrono>
#include <ctime>
#include <iomanip>

#ifdef DEBUG

    #if defined(__i386__) || defined(__x86_64__)
        #define debugbreak() __asm__ __volatile__("int3")
    #elif defined(__aarch64__)
        #define debugbreak() __asm__ __volatile__(".inst 0xd4200000")
    #elif defined(__arm__)
        #define debugbreak() __asm__ __volatile__(".inst 0xe7f001f0")
    #endif

    #define ASSERT(expr)                                \
        if (expr) {}                                    \
        else {                                          \
            Debug::error(#expr, __FILE__, __LINE__);    \
            debugbreak();                               \
        }
    #define DEBUG_ADD_DEPTH() Debug::addDepth()
    #define DEBUG_DEC_DEPTH() Debug::decDepth()
    #define DEBUG_MSG(expr, ...) Debug::msg(expr, ##__VA_ARGS__)
    #define DEBUG_LINE() Debug::line('=')
    #define DEBUG_ERROR(expr) Debug::error((expr))
    #define DEBUG_TIMER_START() Debug::time("start timer"); Timer::start()
    #define DEBUG_TIMER_END() Timer::end(); Debug::time("end timer");
    #define DEBUG_TIMER() Debug::attribute("time spent", std::to_string(Timer::get()), "sec")
    #define DEBUG_ATTR(tag, ...) Debug::attribute(tag, ##__VA_ARGS__)
    #define DEBUG_GEN_LOG() Debug::generateLog()

    #define checkCudaErrors(expr) Debug::checkCuda((expr), #expr, __FILE__, __LINE__)
#else
    #define ASSERT(expr)
    #define DEBUG_ADD_DEPTH()
    #define DEBUG_DEC_DEPTH()
    #define DEBUG_MSG(expr, ...)
    #define DEBUG_LINE()
    #define DEBUG_ERROR(expr)
    #define DEBUG_TIMER_START() 
    #define DEBUG_TIMER_END() 
    #define DEBUG_TIMER()
    #define DEBUG_ATTR(tag, ...)
    #define DEBUG_GEN_LOG()
    #define checkCudaErrors(expr) (expr)
#endif

namespace Debug {

    static const std::string BLACK =         "\033[0;30m";
    static const std::string RED =           "\033[0;31m";
    static const std::string GREEN =         "\033[0;32m";
    static const std::string YELLOW =        "\033[0;33m";
    static const std::string BLUE =          "\033[0;34m";
    static const std::string PURPLE =        "\033[0;35m";
    static const std::string CYAN =          "\033[0;36m";
    static const std::string WHITE =         "\033[0;37m";
    static const std::string RESET_COLOR =   "\033[0m";

    static unsigned int depth = 0;
    static std::stringstream log;

    __host__ inline void indent(std::ostream& output) {
        for (int i = 0; i < depth; i ++) {
            output << "    ";
            log << "    ";
        }
    }

    __host__ inline
    void error(const char* expr, const char* file, int line) {
        indent(std::cerr);
        std::cerr << RED << "[ERROR] " 
                << YELLOW << "[" << file << "] at line " << line 
                << ": " << WHITE << expr << RESET_COLOR << std::endl;
        log << "[ERROR] [" << file << "] at line " << line << ": " << expr << std::endl;
    }

    __host__ inline void msg(std::string expr, std::string color = WHITE) {
        indent(std::cout);
        std::cout << CYAN << "[MSG] " << color << expr << RESET_COLOR << std::endl;
        log << "[MSG] " << expr << std::endl;
    }
    __host__ inline void msg(const char* expr) {
        indent(std::cout);
        std::cout << CYAN << "[MSG] " << WHITE << expr << RESET_COLOR << std::endl;
        log << "[MSG] " << expr << std::endl;
    }

    __host__ inline void error(std::string expr) {
        indent(std::cerr);
        std::cerr << RED << "[ERROR] " << expr << RESET_COLOR << std::endl;
        log << "[ERROR] " << expr << std::endl;
    }

    __host__ inline void error(const char* expr) {
        indent(std::cerr);
        std::cerr << RED << "[ERROR] " << expr << RESET_COLOR << std::endl;
        log << "[ERROR] " << expr << std::endl;
    }

    template <class T>
    __host__ inline void attribute(std::string tag, T value, std::string unit = "") {
        indent(std::cout);        
        std::cout << BLUE << "[ATTR] " << YELLOW << tag << WHITE << ": " << GREEN << value << " " << unit << std::endl;
        log << "[ATTR] " << tag << ": " << value << " " << unit << std::endl;
    }

    __host__ inline void line(char ch) {
        indent(std::cout);
        std::cout << PURPLE;
        for (int i = 0; i < 80 - depth * 4; i ++) {
            std::cout << ch;
            log << ch;
        }
        std::cout << RESET_COLOR << std::endl;
        log << std::endl;
    }

    __host__ inline void time(std::string expr) {
        indent(std::cout);
        std::cout << YELLOW << "[TIME] " << expr << std::endl;
        log << "[TIME] " << expr << std::endl;
    }

    __host__ inline void addDepth() {
        depth ++;
    }

    __host__ inline void decDepth() {
        if (depth) {
            depth --;
        }
    }

    __host__ inline void checkCuda(cudaError_t result, char const* const func, const char* const file, int const line) {
        if (result) {
            error("CUDA: " + static_cast<unsigned int>(result), file, line);
            cudaDeviceReset();
            exit(99);
        }
    }

    __host__ void generateLog() {
        std::string logFile = "./logs/log ";
        auto now = std::chrono::system_clock::now();
        std::time_t time = std::chrono::system_clock::to_time_t(now);
        logFile += std::ctime(&time);
        logFile += ".txt";

        std::ofstream output{logFile};
        output << log.rdbuf();
        output.close();
    }
}

/**
 * @brief This is the namespace for a timer
 * 
 */
namespace Timer {
    static struct timespec startTime;
    static struct timespec endTime;

    /**
     * @brief start the timer
     * 
     */
    __host__ inline void start() {
        clock_gettime(CLOCK_MONOTONIC, &startTime);
    }

    /**
     * @brief end the timer
     * (this should be called after start())
     * 
     */
    __host__ inline void end() {
        clock_gettime(CLOCK_MONOTONIC, &endTime);
    }

    /**
     * @brief get the time passed
     * (this should be called after start() and end())
     * 
     */
    __host__ inline double get() {
        double timeTaken = (endTime.tv_sec - startTime.tv_sec) * 1e9f;
        timeTaken = (timeTaken + (endTime.tv_nsec - startTime.tv_nsec)) * 1e-9f;
        return timeTaken;
    }
}

#endif
