#ifndef IMAGE_MANAGER_CUH
#define IMAGE_MANAGER_CUH

#define STB_IMAGE_IMPLEMENTATION
#include "./external/stb_image.cuh"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "./external/stb_image_write.cuh"

#include "./debug/debug.cuh"
#include "./utilities/utilities.cuh"

class ImageManager {
    int m_width;
    int m_height;
    int m_channels;

    int m_textureWidth;
    int m_textureHeight;
    int m_textureChannels;

    Color* m_image;
    Color* m_texture;

public:
    inline const int&   width() const       { return m_width; }
    inline const int&   height() const      { return m_height; }
    inline const int&   channels() const    { return m_channels; }
    inline const Color* image() const       { return m_image; }
    inline Color*       image()             { return m_image; }
    inline const Color* texture() const     { return m_texture; }
    inline Color*       texture()           { return m_texture; }

public:
    ImageManager(const std::string& file, const std::string& textureFile) {
        DEBUG_MSG("loading file ...");
        DEBUG_ADD_DEPTH();
        DEBUG_ATTR("file", file);

        u_char* image = stbi_load(file.c_str(), &m_width, &m_height, &m_channels, 0);
        if (image == nullptr) {
            throw std::runtime_error("failed to load file: " + file);
        }
        DEBUG_MSG("loaded by stb_image");
        DEBUG_ADD_DEPTH();
        DEBUG_ATTR("width", m_width);
        DEBUG_ATTR("height", m_height);
        DEBUG_ATTR("channels", m_channels);
        DEBUG_DEC_DEPTH();
        DEBUG_DEC_DEPTH();

        allocBuffer();
        convert(image);
        stbi_image_free(image);

        DEBUG_MSG("loading texture ...");
        DEBUG_ADD_DEPTH();
        DEBUG_ATTR("texture", textureFile);

        u_char* texture = stbi_load(textureFile.c_str(), &m_textureWidth, &m_textureHeight, &m_textureChannels, 0);
        if (texture == nullptr) {
            throw std::runtime_error("failed to load file: " + textureFile);
        }

        DEBUG_MSG("loaded by stb_image");
        DEBUG_ADD_DEPTH();
        DEBUG_ATTR("width", m_textureWidth);
        DEBUG_ATTR("height", m_textureHeight);
        DEBUG_ATTR("channels", m_textureChannels);
        DEBUG_DEC_DEPTH();
        DEBUG_DEC_DEPTH();

        genTexture(texture);
        stbi_image_free(texture);

        DEBUG_MSG("image file initialized");
    }

    ~ImageManager() {
        freeBuffer();
    }

    inline void allocBuffer() {
        DEBUG_MSG("allocating image to GPU");
        checkCudaErrors(cudaMallocManaged((void**)&m_image, sizeof(Color) * m_width * m_height));
        DEBUG_MSG("buffer allocated on GPU");
        DEBUG_MSG("allocatin texture to GPU");
        checkCudaErrors(cudaMallocManaged((void**)&m_texture, sizeof(Color) * m_width * m_height));
        DEBUG_MSG("texture allocated on GPU");
    }

    inline void freeBuffer() {
        checkCudaErrors(cudaFree(m_texture));
        checkCudaErrors(cudaFree(m_image));
    }

    inline void convert(u_char* image) {
        DEBUG_MSG("converting stb image to buffer");
        for (int i = 0; i < m_height; i ++) {
            for (int j = 0; j < m_width; j ++) {
                int idx = i * m_width + j;
                m_image[idx].r = static_cast<float>(image[idx * m_channels + 0]) / 256.0f;
                m_image[idx].g = static_cast<float>(image[idx * m_channels + 1]) / 256.0f;
                m_image[idx].b = static_cast<float>(image[idx * m_channels + 2]) / 256.0f;
            }
        }
        DEBUG_MSG("converted");
    }

    inline void genTexture(u_char* texture) {
        DEBUG_MSG("generating texture ...");
        for (int i = 0; i < m_height; i ++) {
            for (int j = 0; j < m_width; j ++) {
                int idx = Utility::getIdx(j, i, m_width);
                m_texture[idx].r = static_cast<float>(texture[Utility::getIdx(j % m_textureWidth, i % m_textureHeight, m_textureWidth) * m_textureChannels + 0]) / 256.0f;
                m_texture[idx].g = static_cast<float>(texture[Utility::getIdx(j % m_textureWidth, i % m_textureHeight, m_textureWidth) * m_textureChannels + 1]) / 256.0f;
                m_texture[idx].b = static_cast<float>(texture[Utility::getIdx(j % m_textureWidth, i % m_textureHeight, m_textureWidth) * m_textureChannels + 2]) / 256.0f;
            }
        }
        DEBUG_MSG("texture generated");
    }

    void exportToImage(const std::string& file) {
        DEBUG_MSG("exporting to image ...");
        DEBUG_ADD_DEPTH();
        DEBUG_ATTR("to file", file);

        int channels = 3;
        u_char* image = (u_char*)malloc(sizeof(u_char) * channels * m_width * m_height);

        for (int i = 0; i < m_height; i ++) {
            for (int j = 0; j < m_width; j ++) {
                int idx = i * m_width + j;
                image[idx * channels + 0] = static_cast<u_char>(m_image[idx].r * 255.9f);
                image[idx * channels + 1] = static_cast<u_char>(m_image[idx].g * 255.9f);
                image[idx * channels + 2] = static_cast<u_char>(m_image[idx].b * 255.9f);
            }
        }

        stbi_write_jpg(file.c_str(), m_width, m_height, channels, image, 0);

        free(image);

        DEBUG_DEC_DEPTH();
        DEBUG_MSG("image exported");
    }
};

#endif
