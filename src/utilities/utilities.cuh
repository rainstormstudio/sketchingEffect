/**
 * @file utilities.cuh
 * @author Hongyu Ding (rainstormstudio@yahoo.com)
 * @brief This file defines some constants and utility helpers
 * @version 0.1
 * @date 2021-06-08
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef UTILITIES_CUH
#define UTILITIES_CUH

#include <cuda.h>
#include <cuda_runtime_api.h>
#include <curand_kernel.h>
#include <time.h>

#include <fstream>
#include <glm/geometric.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/random.hpp>
#include <glm/gtx/compatibility.hpp>
#include <glm/gtx/norm.hpp>
#include <iostream>
#include <limits>
#include <string>
#include <vector>
#include <thread>
#include <exception>

#include "list.cuh"

using Mat4 = glm::mat4;

using Vec4 = glm::vec4;

using Vec3 = glm::vec3;
using Color = glm::vec3;
using Point = glm::vec3;

using Vec3i = glm::ivec3;

using Vec2 = glm::vec2;
using Coord = glm::vec2;
using Vec2i = glm::ivec2;

using Quaternion = glm::quat;

const float INF = std::numeric_limits<float>::infinity();
const float EPSILON = 1e-3f;
const float PI = 3.1415926535897932384626f;
const float INVPI = 0.31830988618f;
const float PIo180 = PI / 180.0f;

#define RANDVEC3 Vec3(curand_uniform(localRandState), curand_uniform(localRandState), curand_uniform(localRandState))

typedef int (*CmpFunc)(void* data, int idxA, int idxB);
typedef void (*SwapFunc)(void* data, int idxA, int idxB);

#define QSORT_STACK_SIZE 32

namespace Utility {
/**
 * @brief converts deg to rad
 * 
 * @param deg 
 * @return float
 */
__host__ __device__ inline float degreesToRadians(float deg) { return deg * PIo180; }

/**
 * @brief remove spaces around str
 * 
 * @param str 
 * @return std::string 
 */
__host__ inline std::string trim(const std::string& str) {
    const auto begin = str.find_first_not_of(' ');
    if (begin == std::string::npos) {
        return "";
    }
    const auto end = str.find_last_not_of(' ');
    const auto range = end - begin + 1;

    return str.substr(begin, range);
}

/**
 * @brief checks if str is a natural number
 * 
 * @param str 
 * @return true 
 * @return false 
 */
__host__ inline bool isNat(const std::string& str) {
    int integer = 0;
    try {
        integer = std::stoi(str);
    } catch (...) {
        return false;
    }
    if (integer >= 0) return true;
    return false;
}

/**
 * @brief checks if str is a valid float
 * 
 * @param str 
 * @return true
 * @return false
 */
__host__ inline bool isFloat(const std::string& str) {
    float ans = 0.0f;
    try {
        ans = std::stof(str);
    } catch (...) {
        return false;
    }
    if (ans == INF) return false;
    return true;
}

/**
 * @brief converts str to lowercase
 * 
 * @param str 
 * @return std::string
 */
__host__ inline std::string lowercase(const std::string& str) {
    std::string lower;
    for (char ch : str) {
        if (ch >= 'A' && ch <= 'Z') {
            lower += ch - 'A' + 'a';
        } else {
            lower += ch;
        }
    }
    return lower;
}

/**
 * @brief checks if the given str is a bool
 * 
 * @param str 
 * @return true
 * @return false
 */
__host__ inline bool isBool(const std::string& str) {
    std::string v = lowercase(str);
    return (v == "true" || v == "t" || v == "false" || v == "f");
}

/**
 * @brief convert value to bool
 * (TRUE => true)
 * 
 * @param value 
 * @return true 
 * @return false 
 */
__host__ inline bool toBool(const std::string& value) {
    std::string v = lowercase(value);
    if (v == "true" || v == "t") return true;
    return false;
}

/**
 * @brief checks if value is a float
 * 
 * @param value 
 * @return true
 * @return false
 */
__host__ inline bool isVec3(const std::string& value) {
    int len = value.length();
    if (value[0] != '(' || value[len - 1] != ')') return false;

    std::string vec[3];
    size_t idx = 0;
    for (int i = 1; i < len - 1; i++) {
        if (value[i] == ',') {
            idx++;
            continue;
        }
        if (value[i] == ' ') continue;
        vec[idx] += value[i];
    }
    if (idx != 2) return false;
    for (int i = 0; i < 3; i++) {
        if (!isFloat(vec[i])) return false;
    }
    return true;
}

/**
 * @brief converts value to a vec3
 * 
 * @param value 
 * @return Vec3
 */
__host__ inline Vec3 to_vec3(const std::string& value) {
    int len = value.length();
    std::string vec[3];
    size_t idx = 0;
    for (int i = 1; i < len - 1; i++) {
        if (value[i] == ',') {
            idx++;
            continue;
        }
        if (value[i] == ' ') continue;
        vec[idx] += value[i];
    }
    Vec3 ans = Vec3(std::stof(vec[0]), std::stof(vec[1]), std::stof(vec[2]));
    return ans;
}

__host__ __device__ inline 
int getIdx(const int& x, const int& y, const int& w) {
    return y * w + x;
}

/**
 * @brief swap x and y
 * 
 * @param x 
 * @param y 
 */
template <class T>
__host__ __device__ inline void swap(T& x, T& y) {
    T tmp = x;
    x = y;
    y = tmp;
}

template <class T>
__host__ __device__ inline T max(const T& a, const T& b) {
    if (a > b) return a;
    return b;
}

template <class T>
__host__ __device__ inline T min(const T& a, const T& b) {
    if (a < b) return a;
    return b;
}

template <class T>
__host__ __device__ inline T min(T& a, T& b, T& c) { return min(a, min(b, c)); }

/**
 * @brief random in unit sphere
 * 
 * @param localRandState 
 * @return Vec3
 */
__device__ inline Vec3 randUnitSphere(curandState* localRandState) {
    Vec3 p;
    do {
        p = 2.0f * RANDVEC3 - Vec3(1, 1, 1);
    } while (glm::length2(p) >= 1.0f);
    return p;
}

/**
 * @brief random in unit disk
 * 
 * @param localRandState 
 * @return Vec3
 */
__device__ inline Vec3 randUnitDisk(curandState* localRandState) {
    Vec3 p;
    do {
        p = 2.0f * Vec3(curand_uniform(localRandState), curand_uniform(localRandState), 0.0f) - Vec3(1.0f, 1.0f, 0.0f);
    } while (glm::dot(p, p) >= 1.0f);
    return p;
}

/**
 * @brief random cosine direction
 * 
 * @param localRandState 
 * @return Vec3
 */
__device__ inline Vec3 randCosDirection(curandState* localRandState) {
    float r1 = curand_uniform(localRandState);
    float r2 = curand_uniform(localRandState);
    float z = sqrtf(1.0f - r2);

    float phi = 2.0f * PI * r1;
    float x = cosf(phi) * sqrtf(r2);
    float y = sinf(phi) * sqrtf(r2);

    return Vec3(x, y, z);
}

__device__ inline float randf(curandState* localRandState, float a, float b) {
    if (b > a) swap(a, b);
    return curand_uniform(localRandState) * (b - a) + a;
}

__device__ inline int randi(curandState* localRandState, int a, int b) {
    if (b > a) swap(a, b);
    int range = b - a + 1;
    return static_cast<int>(round(curand_uniform(localRandState) * range + a)) - 1;
}

/**
 * @brief calculates the reflected vector based on v and n
 * 
 * @param v incident vector
 * @param n normal vector
 * @return Vec3
 */
__host__ __device__ inline Vec3 reflect(const Vec3& v, const Vec3& n) {
    return v - 2 * glm::dot(v, n) * n;
}

/**
 * @brief calculates the refracted vector based on uv and n
 * 
 * @param uv normalized v
 * @param n normal
 * @param etai_over_etat 
 * @return Vec3
 */
__host__ __device__ inline Vec3 refract(const Vec3& uv, const Vec3& n, float etai_over_etat) {
    auto cos_theta = glm::dot(-uv, n);
    Vec3 ray_out_perp = etai_over_etat * (uv + cos_theta * n);
    Vec3 ray_out_parallel = -sqrtf(fabs(1.0 - glm::length2(ray_out_perp))) * n;
    return ray_out_perp + ray_out_parallel;
}

/**
 * @brief checks if v is refracted based on n and calculates refracted vector
 * 
 * @param v incident vector
 * @param n normal vector
 * @param ni_over_nt 
 * @param refracted output refracted vector
 * @return true
 * @return false
 */
__host__ __device__ inline bool refract(const Vec3& v, const Vec3& n, float ni_over_nt, Vec3& refracted) {
    Vec3 uv = glm::normalize(v);
    float dt = glm::dot(uv, n);
    float discriminant = 1.0f - ni_over_nt * ni_over_nt * (1.0f - dt * dt);
    if (discriminant > 0.0f) {
        refracted = ni_over_nt * (uv - n * dt) - n * sqrt(discriminant);
        return true;
    }
    return false;
}

/**
 * @brief prints vec
 * 
 * @param vec 
 */
__host__ __device__ inline void printVec3(const Vec3& vec) {
    printf("(%f, %f, %f)", vec.x, vec.y, vec.z);
}

/**
 * @brief converts vec to string
 * 
 * @param vec 
 * @return string
 */
__host__ inline std::string to_string(const Vec3& vec) {
    return "(" + std::to_string(vec.x) + ", " + std::to_string(vec.y) + ", " + std::to_string(vec.z) + ")";
}

/**
 * @brief checks if vec is near zero
 * 
 * @param vec 
 * @return true
 * @return false
 */
__host__ __device__ inline bool isZero(const Vec3& vec) {
    return glm::all(glm::lessThan(glm::abs(vec), glm::vec3(EPSILON)));
}

__host__ __device__ inline float triangleArea(const Vec3& a, const Vec3& b, const Vec3& c) {
    Vec3 ab = b - a;
    Vec3 ac = c - a;
    float lab = glm::length(ab);
    float lac = glm::length(ac);

    float cosine = glm::dot(ab, ac) / (lab * lac);
    return 0.5f * lab * lac * sqrtf(1.0f - cosine * cosine);
}

__host__ __device__ inline void insertionSort(int start, int end, void* data, CmpFunc cmpFunc, SwapFunc swapFunc) {
    for (int i = 1; i < end; i++) {
        int j = start + i - 1;
        while (j >= start && cmpFunc(data, j, j + 1) > 0) {
            swapFunc(data, j, j + 1);
            j--;
        }
    }
}

__host__ __device__ inline int getPivot(int start, int end, void* data, CmpFunc cmpFunc) {
    int low = start;
    int tmp = (start + end) >> 1;
    int high = end - 2;
    if (cmpFunc(data, low, high) > 0) swap(low, high);
    if (cmpFunc(data, low, tmp) > 0) tmp = low;
    return (cmpFunc(data, tmp, high) > 0) ? high : tmp;
}

__host__ __device__ inline void qsort(int start, int end, void* data, CmpFunc cmpFunc, SwapFunc swapFunc) {
    int stack[QSORT_STACK_SIZE];
    int stackPtr = 0;
    stack[stackPtr++] = end;

    while (stackPtr) {
        end = stack[--stackPtr];

        if (end - start <= 15 || stackPtr + 2 > QSORT_STACK_SIZE) {
            insertionSort(start, end - start, data, cmpFunc, swapFunc);
            start = end + 1;
            continue;
        }

        swapFunc(data, getPivot(start, end, data, cmpFunc), end - 1);

        int i = start - 1;
        int j = end - 1;
        while (1) {
            do i++;
            while (cmpFunc(data, i, end - 1) < 0);
            do j--;
            while (cmpFunc(data, j, end - 1) > 0);
            if (i >= j) break;
            swapFunc(data, i, j);
        }

        swapFunc(data, i, end - 1);

        if (end - i > 2) stack[stackPtr++] = end;
        if (i - start > 1)
            stack[stackPtr++] = i;
        else
            start = i + 1;
    }
}

__host__ __device__ inline void sort(int start, int end, void* data, CmpFunc cmpFunc, SwapFunc swapFunc) {
    if (start + 2 <= end) qsort(start, end, data, cmpFunc, swapFunc);
}

__host__ __device__ inline int cmpINT(void* data, int idxA, int idxB) {
    int a = ((int*)data)[idxA];
    int b = ((int*)data)[idxB];
    return (a < b) ? -1 : (a > b) ? 1
                                  : 0;
}

__host__ __device__ inline void swapINT(void* data, int idxA, int idxB) {
    swap(((int*)data)[idxA], ((int*)data)[idxB]);
}

__host__ __device__ inline int cmpFLOAT(void* data, int idxA, int idxB) {
    float a = ((float*)data)[idxA];
    float b = ((float*)data)[idxB];
    return (a < b) ? -1 : (a > b) ? 1
                                  : 0;
}

__host__ __device__ inline void swapFLOAT(void* data, int idxA, int idxB) {
    swap(((float*)data)[idxA], ((float*)data)[idxB]);
}

__host__ __device__ inline int asINT(const float& x) { return *(int*)&x; }
__host__ __device__ inline float asFLOAT(const int& x) { return *(float*)&x; }

__host__ __device__ inline float luminance(const Color& color) {
    return 0.2126f * color.r + 0.7152f * color.g + 0.0722 * color.b;
}

__host__ __device__ inline void gammaCorrection(Vec3& color, const float& gamma) {
    const float tmp = 1.0f / gamma;
    color.r = powf(color.r, tmp);
    color.g = powf(color.g, tmp);
    color.b = powf(color.b, tmp);
}

__host__ __device__ inline void removeNaN(float& value) {
    if (value != value) value = 0.0f;
}

__host__ __device__ inline void removeNaN(Vec3& vec) {
    removeNaN(vec.r);
    removeNaN(vec.g);
    removeNaN(vec.b);
}

}  // namespace Utility

/**
 * @brief Orthonormal Basis
 * 
 */
class ONB {
    Vec3 _axis[3];
public:
    __device__ inline ONB() {}
    __device__ inline ONB(const Vec3& normal) { build(normal); }

    __device__ inline const Vec3&   operator[](int idx) const   { return _axis[idx]; }
    __device__ inline Vec3&         operator[](int idx)         { return _axis[idx]; }
    
    __device__ inline const Vec3&   u() const   { return _axis[0]; }
    __device__ inline Vec3&         u()         { return _axis[0]; }
    __device__ inline const Vec3&   v() const   { return _axis[1]; }
    __device__ inline Vec3&         v()         { return _axis[1]; }
    __device__ inline const Vec3&   w() const   { return _axis[2]; }
    __device__ inline Vec3&         w()         { return _axis[2]; }

    __device__ inline Vec3 local(float a, float b, float c) const   { return a * u() + b * v() + c * w(); }
    __device__ inline Vec3 local(const Vec3& vec) const             { return vec.x * u() + vec.y * v() + vec.z * w(); }

    __device__ inline void build(const Vec3& normal) {
        w() = glm::normalize(normal);
        Vec3 a = (fabsf(w().x - 1.0f) < EPSILON) ? Vec3(0.0f, 1.0f, 0.0f) : Vec3(1.0f, 0.0f, 0.0f);
        v() = glm::normalize(glm::cross(w(), a));
        u() = glm::cross(w(), v());
    }
};

#endif
