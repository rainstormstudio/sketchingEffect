/**
 * @file list.cuh
 * @author Hongyu Ding (rainstormstudio@yahoo.com)
 * @brief This file defines a list
 * @version 0.1
 * @date 2021-06-10
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef LIST_CUH
#define LIST_CUH

#include <iostream>

/**
 * @brief copy from src to dst with size of size
 * 
 * @tparam T 
 * @param dst 
 * @param src 
 * @param size 
  */
template<class T> __host__ __device__
void copy(T* dst, const T* src, size_t size);

/**
 * @brief copy overlap from src to dst
 * 
 * @tparam T 
 * @param dst 
 * @param src 
 * @param size 
  */
template<class T> __host__ __device__
void copyOverlap(T* dst, const T* src, size_t size);

template<class T>
class List {
    const size_t MIN_SIZE = 32;

    T* _data;
    size_t _size;
    size_t _capacity;
public:
    __host__ __device__ void                init() { _data = nullptr; _size = 0; _capacity = 0; }
    __host__ __device__                     List() { init(); }
    __host__ __device__ explicit            List(const T& item) {init(); push_back(item); }
    __host__ __device__                     List(const T* data, size_t size) { init(); set(data, size); }
    __host__ __device__                     List(const List<T>& other) { init(); set(other); }
    __host__ __device__                     ~List() { delete [] _data; }

    __host__ __device__ inline const T*     data() const { return _data; }
    __host__ __device__ inline T*           data() { return _data; }
    __host__ __device__ inline size_t       size() const { return _size; }
    __host__ __device__ inline size_t       capcaity() const { return _capacity; }

    __host__ __device__ inline const T&     get(size_t idx) const { return _data[idx]; }
    __host__ __device__ inline T&           get(size_t idx) { return _data[idx]; }

    __host__ __device__ T       set(size_t idx, const T& item) { T& slot = get(idx); T old = slot; slot = item; return old; }
    __host__ __device__ void    set(const T* ptr, size_t size) { reset(size); if (ptr) copy(getPtr(), ptr, size); }
    __host__ __device__ void    set(const List<T>& other) { if (&other != this) set(other.getPtr(), other.size()); }

    __host__ __device__ inline const T*     getPtr(size_t idx = 0) const { return _data + idx; }
    __host__ __device__ inline T*           getPtr(size_t idx = 0) { return _data + idx; }

    __host__ __device__ inline const T&     first() const { return get(0); }
    __host__ __device__ inline T&           first() { return get(0); }

    __host__ __device__ inline const T&     last() const { return get(size() - 1); }
    __host__ __device__ inline T&           last() { return get(size() - 1); }

    __host__ __device__ inline int          typeSize() const { return sizeof(T); }
    __host__ __device__ inline int          numBytes() const { return size() * typeSize(); }

    __host__ __device__ void setCapacity(size_t capacity) { size_t tmp = max(capacity, _size); if (_capacity != tmp) reallocate(tmp); }
    __host__ __device__ void compact() { setCapacity(0); }
    __host__ __device__ void reset(size_t size = 0) { clear(); setCapacity(size); _size = size; }
    __host__ __device__ void clear() { _size = 0; }
    __host__ __device__ void resize(size_t size);

    __host__ __device__ inline T    remove(size_t idx) { T old = get(idx); replace(idx, idx + 1, 0); return old; }
    __host__ __device__ inline void remove(size_t start, size_t end) { replace(start, end, 0); }

    __host__ __device__ inline T&   popLast() { _size --; return _data[_size]; }

    __host__ __device__ T& push_back(const T& item) { T* slot = push_back(nullptr, 1); *slot = item; return *slot; }
    __host__ __device__ T* push_back(const T* data, size_t size);

    __host__ __device__ T* append(const List<T>& other) { return replace(size(), size(), other); }

    __host__ __device__ inline const T&     operator[](size_t idx) const { return get(idx); }
    __host__ __device__ inline T&           operator[](size_t idx) { return get(idx); }    
    __host__ __device__ List<T>&            operator=(const List<T>& other) { set(other); return *this; }

    __host__ __device__ T* replace(size_t start, size_t end, size_t size);
    __host__ __device__ T* replace(size_t start, size_t end, const T* ptr, size_t size) { T* slot = replace(start, end, size); if (ptr) copy(slot, ptr, size); return slot; }
    __host__ __device__ T* replace(size_t start, size_t end, const List<T>& other);

    __host__ __device__ void reallocate(size_t size);
};

template<class T> __host__ __device__
void List<T>::resize(size_t size) {
    if (size > _capacity) {
        size_t allocSize = max(static_cast<int>(MIN_SIZE / sizeof(T)), 1);
        while (size > allocSize) allocSize <<= 1;
        reallocate(allocSize);
    }
    _size = size;
}

template<class T> __host__ __device__
void List<T>::reallocate(size_t size) {
    T* newData = nullptr;
    if (size > 0) {
        newData = new T[size];
        copy(newData, _data, min(size, _size));
    }
    delete [] _data;
    _data = newData;
    _capacity = size;
}

template<class T> __host__ __device__ 
T* List<T>::push_back(const T* data, size_t size) { 
    size_t oldSize = _size; 
    resize(oldSize + size); 
    T* slot = getPtr(oldSize); 
    if (data) copy<T>(slot, data, size); 
    return slot; 
}

template<class T> __host__ __device__
void copy(T* dst, const T* src, size_t size) {
    if (size <= 0) return;
    for (size_t i = 0; i < size; i ++)
        dst[i] = src[i];
}

template<class T> __host__ __device__
void copyOverlap(T* dst, const T* src, size_t size) {
    if (!size) return;
    if (dst < src || dst >= src + size) {
        for (size_t i = 0; i < size; i ++) dst[i] = src[i];
    } else {
        for (int i = size - 1; i >= 0; i --) dst[i] = src[i];
    }
}

template <class T> __host__ __device__
T* List<T>::replace(size_t start, size_t end, size_t size) {
	int tailSize = _size - end;
	int newEnd = start + size;
	resize(_size + newEnd - end);

	copyOverlap(_data + newEnd, _data + end, tailSize);
	return _data + start;
}

template <class T> __host__ __device__
T* List<T>::replace(size_t start, size_t end, const List<T>& other)
{
	List<T> tmp;
	const T* ptr = other.data();
	if (&other == this) {
		tmp = other;
		ptr = tmp.data();
	}
	return replace(start, end, ptr, other.size());
}


#endif
