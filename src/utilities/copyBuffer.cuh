#ifndef COPY_BUFFER_CUH
#define COPY_BUFFER_CUH


#include "utilities.cuh"

__global__ void copyBuffer(Color* dst, Color* src, int width, int height) {
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    if ((x >= width) || (y >= height)) return;

    int idx = Utility::getIdx(x, y, width);

    dst[idx] = src[idx];
}

#endif
