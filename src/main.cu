#include <cstdlib>

#include "./utilities/utilities.cuh"
#include "./utilities/copyBuffer.cuh"
#include "./debug/debug.cuh"
#include "imageManager.cuh"
#include "./filters/grayscale.cuh"
#include "./filters/gradient.cuh"
#include "./filters/classification.cuh"
#include "./filters/convolution.cuh"
#include "./filters/invert.cuh"
#include "./filters/lineShaping.cuh"
#include "./filters/createKernel.cuh"
#include "./filters/toneTransfer.cuh"
#include "./filters/mixture.cuh"
#include "./filters/applyTexture.cuh"
#include "./filters/adjustBrightness.cuh"

int main(int argv, char* argc[]) {
    DEBUG_LINE();
    DEBUG_MSG("start program");
    if (argv != 3) {
        std::cerr << "no file input" << std::endl;
        return EXIT_FAILURE;
    }

    std::string file = std::string(argc[1]);
    std::string textureFile = std::string(argc[2]);

    try {
        ImageManager loader{file, textureFile};
        DEBUG_LINE();
        DEBUG_MSG("processing ...");
        DEBUG_ADD_DEPTH();

        u_int32_t blockWidth = 8;
        u_int32_t blockHeight = 8;

        dim3 blocks{static_cast<u_int32_t>(loader.width()) / blockWidth + 1, static_cast<u_int32_t>(loader.height()) / blockHeight + 1};
        dim3 threads{blockWidth, blockHeight};

        DEBUG_TIMER_START();

        DEBUG_MSG("apply grey scale");
        grayScale<<<blocks, threads>>>(loader.image(), loader.width(), loader.height());
        grayScale<<<blocks, threads>>>(loader.texture(), loader.width(), loader.height());
        checkCudaErrors(cudaGetLastError());
        checkCudaErrors(cudaDeviceSynchronize());

        Color* gray;
        checkCudaErrors(cudaMalloc((void**)&gray, sizeof(Color) * loader.width() * loader.height()));
        copyBuffer<<<blocks, threads>>>(gray, loader.image(), loader.width(), loader.height());
        checkCudaErrors(cudaGetLastError());
        checkCudaErrors(cudaDeviceSynchronize());

        Color* gradientBuffer;
        checkCudaErrors(cudaMallocManaged((void**)&gradientBuffer, sizeof(Color) * loader.width() * loader.height()));
        
        DEBUG_MSG("apply gradient mapping");
        gradient<<<blocks, threads>>>(loader.image(), gradientBuffer, loader.width(), loader.height());
        checkCudaErrors(cudaGetLastError());
        checkCudaErrors(cudaDeviceSynchronize());

        DEBUG_MSG("generate kernel");
        const int kernelSize = 3;
        int kernelWidth = kernelSize * 2 + 1;
        Color* kernel;
        checkCudaErrors(cudaMalloc((void**)&kernel, sizeof(Color) * kernelWidth * kernelWidth * 8));
        for (int dir = 0; dir < 8; dir ++) {
            createKernel<<<1, 1>>>(&kernel[dir * kernelWidth * kernelWidth], kernelSize, dir);
        }
        checkCudaErrors(cudaGetLastError());
        checkCudaErrors(cudaDeviceSynchronize());

        DEBUG_MSG("apply convolution");
        Color* conv;
        checkCudaErrors(cudaMalloc((void**)&conv, sizeof(Color) * loader.width() * loader.height() * 8));
        for (int dir = 0; dir < 8; dir ++) {
            convolution<<<blocks, threads>>>(gradientBuffer, &conv[dir * loader.width() * loader.height()], loader.width(), loader.height(), &kernel[dir * kernelWidth * kernelWidth], kernelSize);
        }
        checkCudaErrors(cudaGetLastError());
        checkCudaErrors(cudaDeviceSynchronize());

        DEBUG_MSG("apply classification");
        classification<<<blocks, threads>>>(conv, gradientBuffer, loader.width(), loader.height());
        checkCudaErrors(cudaGetLastError());
        checkCudaErrors(cudaDeviceSynchronize());
        
        DEBUG_MSG("apply convolution");
        Color* tmpBuffer;
        checkCudaErrors(cudaMalloc((void**)&tmpBuffer, sizeof(Color) * loader.width() * loader.height() * 8));
        for (int dir = 0; dir < 8; dir ++) {
            convolution<<<blocks, threads>>>(&conv[dir * loader.width() * loader.height()], &tmpBuffer[dir * loader.width() * loader.height()], loader.width(), loader.height(), &kernel[dir * kernelWidth * kernelWidth], kernelSize);
        }
        checkCudaErrors(cudaGetLastError());
        checkCudaErrors(cudaDeviceSynchronize());

        DEBUG_MSG("apply line shaping");
        lineShaping<<<blocks, threads>>>(tmpBuffer, loader.image(), loader.width(), loader.height());
        checkCudaErrors(cudaGetLastError());
        checkCudaErrors(cudaDeviceSynchronize());

        DEBUG_MSG("apply invert");
        invert<<<blocks, threads>>>(loader.image(), loader.width(), loader.height());
        checkCudaErrors(cudaGetLastError());
        checkCudaErrors(cudaDeviceSynchronize());

        DEBUG_MSG("apply tone transfer");
        toneTransfer<<<blocks, threads>>>(gray, loader.width(), loader.height());
        checkCudaErrors(cudaGetLastError());
        checkCudaErrors(cudaDeviceSynchronize());

        DEBUG_MSG("apply texture");
        applyTexture<<<blocks, threads>>>(gray, loader.texture(), loader.width(), loader.height());
        checkCudaErrors(cudaGetLastError());
        checkCudaErrors(cudaDeviceSynchronize());

        DEBUG_MSG("mix buffers");
        mixture<<<blocks, threads>>>(loader.image(), gray, loader.width(), loader.height());
        checkCudaErrors(cudaGetLastError());
        checkCudaErrors(cudaDeviceSynchronize());

        DEBUG_MSG("adjust brightness");
        adjustBrightness<<<blocks, threads>>>(loader.image(), loader.width(), loader.height());
        checkCudaErrors(cudaGetLastError());
        checkCudaErrors(cudaDeviceSynchronize());

        DEBUG_TIMER_END();
        DEBUG_TIMER();

        checkCudaErrors(cudaFree(tmpBuffer));
        checkCudaErrors(cudaFree(gradientBuffer));
        checkCudaErrors(cudaFree(conv));
        checkCudaErrors(cudaFree(kernel));

        DEBUG_DEC_DEPTH();
        DEBUG_MSG("finish");
        DEBUG_LINE();

        std::string outputFile = "output.jpg";
        loader.exportToImage(outputFile);
    } catch (const std::exception& e) {
        DEBUG_ERROR(e.what());
        return EXIT_FAILURE;
    }

    DEBUG_MSG("end program");
    DEBUG_LINE();

    DEBUG_GEN_LOG();

    return EXIT_SUCCESS;
}